const searchForm = document.querySelector('[data-js="search-form"')
const queryInput = document.querySelector('#query')
const queryTypeSelect = document.querySelector('#query-type')
const searchButton = document.querySelector('[data-js="search-btn"]')
const personTable = document.querySelector('[data-js="person-table"]')
const warningMessageSpan = document.querySelector('[data-js="warning-message"]')
const peopleLength = document.querySelector('[data-js="people-length"]')

queryTypeSelect.addEventListener('change', () => changeQueryInputPlaceholder())

const changeQueryInputPlaceholder = () => {
  const types = {
    nome: 'John doe',
    cpf: '012.334.921-00',
    rg: '10.072.732-3',
    data_nasc: '23/10/1996',
    sexo: 'Feminino'
  }

  queryInput.setAttribute('placeholder', `Ex: ${types[queryTypeSelect.value]}`)
}

const handleSubmit = async e => {
  e.preventDefault()
  warningMessageSpan.textContent = ''

  const type = queryTypeSelect.value
  const query = queryInput.value

  try {
    const response = await fetch(`/people?${type}=${query}`)
    const peopleJson = await response.json()

    createPeopleList(peopleJson)
  } catch (error) {
    console.error(error)

    warningMessageSpan.setAttribute('data-error', 'error')
    warningMessageSpan.textContent =
      'Algo deu errado na busca, tente novamente!'
  }
}

const createPeopleList = peopleList => {
  const template = peopleList.reduce(
    (acc, cur) => acc + createPersonTemplate(cur),
    ''
  )

  if (!template) {
    warningMessageSpan.textContent = 'Nenhuma pessoa encontrada.'
  }

  peopleLength.textContent = `Quantidade de pessoas encontradas: ${peopleList.length}`
  personTable.innerHTML = template
}

const createPersonTemplate = person =>
  `<tr>
    <td>${person.nome}</td>
    <td>${person.idade}</td>
    <td>${person.cpf}</td>
    <td>${person.rg}</td>
    <td>${person.data_nasc}</td>
    <td>${person.sexo}</td>
  </tr>
  `

searchForm.addEventListener('submit', handleSubmit)

window.addEventListener('load', () => changeQueryInputPlaceholder())
