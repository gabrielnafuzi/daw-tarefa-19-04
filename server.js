const express = require('express')
const routes = require('./routes')

const PORT = 3000

const app = express()

app.use(express.static(__dirname + '/public'))
app.use(express.urlencoded({ extended: true }))
app.use(routes)

app.listen(PORT, () => {
  console.log(`🚀 Servidor está rodando em: http://localhost:${PORT}`)
})
