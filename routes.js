const { Router } = require('express')
const people = require('./db.json')
const routes = Router()

routes.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
})

routes.get('/people', (req, res) => {
  const key = Object.keys(req.query)[0]
  const query = req.query[key].toLowerCase()

  const filteredPeople = people.filter(person =>
    person[key].toLowerCase().includes(query)
  )

  res.status(200).json(filteredPeople)
})

module.exports = routes
